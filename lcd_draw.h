#ifndef __DRAW__
#define __DRAW__

#include <string.h>

#include "hw_utils.h"
#include "sw_utils.h"
#include "font_types.h"

#define FONT font_rom8x16

typedef struct colors_st
{
    pixel_st black;
    pixel_st brown;
    pixel_st white;
} colors_st;

void init_colors();

void draw_rect(int xstart, int ystart, int xend, int yend, pixel_st color);

void draw_line(int xstart, int ystart, int xend, int yend, int width, pixel_st color);

void draw_circle(int x, int y, int r, pixel_st color);

//returns the width of printed char after scaleing
int lcd_prnt_char(int x, int y, unsigned char c, float scale, pixel_st color);

void lcd_prnt_str(int xstart, int ystart, char *str, float scale, pixel_st color, int maxwidth);

void lcd_prnt_int(int xstart, int ystart, int num, float scale, pixel_st color);

#endif