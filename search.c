#include <stdbool.h>
#include <stdio.h>

#include "search.h"

extern colors_st colors;
extern city_st slots[NO_OF_SLOTS];

extern pthread_mutex_t data_mtx;
extern pthread_cond_t data_cond;

void *autocomplete_thread(void *arg)
{
    autocomp *autocomplete = (autocomp *)arg;
    while (true)
    {
        pthread_mutex_lock(&data_mtx);
        pthread_cond_wait(&data_cond, &data_mtx);
        if (autocomplete->quit)
        {
            pthread_mutex_unlock(&data_mtx);
            break;
        }
        pthread_mutex_unlock(&data_mtx);
        get_autocomplete_data(autocomplete->input, autocomplete->limit);
        pthread_mutex_lock(&data_mtx);
        if (!parse_autocomplete(AUTOCOMPLETE_PATH, autocomplete))
        {
            printf("weather parse failed, please contact customer support (734 526 149)\n");
        }
        draw_rect(0, 5 + FONT.height * 1.6, LCD_WIDTH, LCD_HEIGHT - 165 - FONT.height * 1.3, colors.black);
        int i = 0;
        while ((i < 3) && (i < autocomplete->cities_quant))
        {
            lcd_prnt_str(5, 5 + FONT.height * (1.6 + i * 1.3), autocomplete->cities[i].name, 1.3f, colors.white, FALSE);
            i++;
        }
        pthread_mutex_unlock(&data_mtx);
    }
    return NULL;
}

//select from cities provided by autocomplete
city_st *select_name(int slot_id, autocomp autocomplete)
{
    city_st *ret = NULL;
    if (autocomplete.cities_quant)
    {
        int selected = (get_knob_state(GREEN) / 4) % autocomplete.cities_quant;
        fill_display(colors.black);
        lcd_prnt_str(5, 5, "Available", 1.6, colors.white, FALSE);
        while (!get_knob_button_state(GREEN))
        {
            selected = (get_knob_state(GREEN) / 4) % autocomplete.cities_quant;
            for (int i = 0; i < autocomplete.cities_quant; ++i)
            {
                pixel_st color = i == selected ? colors.brown : colors.white;
                lcd_prnt_str(5, 5 + FONT.height * (1.6 + i * 1.3), autocomplete.cities[i].name, 1.3f, color, FALSE);
            }
            update_display();
        }
        while (get_knob_button_state(GREEN))
            ;
        slots[slot_id] = autocomplete.cities[selected];
        ret = &slots[slot_id];
    }
    return ret;
}

void search(autocomp *autocomp)
{
    pthread_mutex_lock(&data_mtx);
    autocomp->input[0] = 0;
    pthread_mutex_unlock(&data_mtx);

    fill_display(colors.black);
    float scale = 1.3f;
    int cursorx = 150;
    int cursory = LCD_HEIGHT - 160 - FONT.height * scale - 2;
    char c = 1;
    lcd_prnt_str(5, 5, "Available", 1.6, colors.white, FALSE);
    lcd_prnt_str(5, cursory, "Searching:", 1.6, colors.white, FALSE);
    int string_ptr = 0;
    while (c != 0)
    {
        draw_rect(cursorx, cursory, cursorx + FONT.maxwidth * scale + 2, cursory + FONT.height * scale, colors.white);
        c = keyboard();
        draw_rect(cursorx, cursory, cursorx + FONT.maxwidth * scale + 2, cursory + FONT.height * scale, colors.black);
        if (c == '\b' && cursorx > 150)
        {
            cursorx -= FONT.maxwidth * scale + 1;
            if (cursorx < 150)
            {
                cursorx = 150;
            }
            if (string_ptr)
            {
                pthread_mutex_lock(&data_mtx);
                autocomp->input[--string_ptr] = '\0';
                pthread_mutex_unlock(&data_mtx);
                pthread_cond_signal(&data_cond);
            }
        }
        else if (c != '\0' && c != '\b' && string_ptr < 19)
        {
            lcd_prnt_char(cursorx, cursory, c, scale, colors.white);
            cursorx += FONT.maxwidth * scale + 1;
            pthread_mutex_lock(&data_mtx);
            autocomp->input[string_ptr++] = c;
            autocomp->input[string_ptr] = '\0';
            pthread_mutex_unlock(&data_mtx);
            pthread_cond_signal(&data_cond);
        }
    }
}

city_st choose_city(autocomp *autocomplete)
{
    fill_display(colors.black);
    lcd_prnt_str(5, 5, "Choose a place.", 2, colors.white, FALSE);
    lcd_prnt_str(5, LCD_HEIGHT - FONT.height * 2, "green knob to navigate and select", 1, colors.white, FALSE);
    lcd_prnt_str(5, LCD_HEIGHT - FONT.height, "red button to search for new city", 1, colors.white, FALSE);
    draw_rect(0, 7 + FONT.height * 2, LCD_WIDTH, 9 + FONT.height * 2, colors.white);
    int selected = (get_knob_state(GREEN) / 4) % NO_OF_SLOTS;
    pixel_st color;
    while (TRUE)
    {
        selected = (get_knob_state(GREEN) / 4) % NO_OF_SLOTS;
        for (int i = 0; i < NO_OF_SLOTS; ++i)
        {
            color = (i == selected) ? colors.brown : colors.white;
            if (slots[i].id)
            {
                lcd_prnt_str(10, 10 + 2 * FONT.height + (i * 1.3 * (FONT.height + 1)), slots[i].name, 1.3, color, FALSE);
            }
            else
            {
                lcd_prnt_str(10, 10 + 2 * FONT.height + (i * 1.3 * (FONT.height + 1)), "empty slot - search for city", 1.3, color, FALSE);
            }
        }
        update_display();
        if (get_knob_button_state(GREEN))
        {
            break;
        }
        else if (get_knob_button_state(RED))
        {
            slots[selected].id = FALSE;
            break;
        }
    }
    while (get_knob_button_state(RED) || get_knob_button_state(GREEN))
        ;
    /*wait until button is released*/
    while (!slots[selected].id)
    {
        search(autocomplete);
        select_name(selected, *autocomplete);
    }
    return slots[selected];
}

void cities_init()
{
    for (int i = 0; i < NO_OF_SLOTS; ++i)
    {
        slots[i].id = 0;
        slots[i].name[0] = '0';
    }
}