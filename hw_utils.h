#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#ifndef __HW_UTILS__
#define __HW_UTILS__

#define LCD_WIDTH 480
#define LCD_HEIGHT 320

typedef union pixel_st
{
	struct
	{
		unsigned b : 5;
		unsigned g : 6;
		unsigned r : 5;
	} data;
	uint16_t rgb;
} pixel_st;

typedef struct rgb_st
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgb_st;

enum knobs
{
	BLUE,
	GREEN,
	RED
};

void fill_display(pixel_st color);

void rgb_led(const char led_num, rgb_st color);

void led_line(const char leds[32]);

_Bool hw_init();

void update_display();

void pixel_write(const int x, const int y, const pixel_st color);

uint8_t get_knob_state(const int knob_color);

_Bool get_knob_button_state(const int knob_color);

#endif