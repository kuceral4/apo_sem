#ifndef __PARSE_JSON_H__
#define __PARSE_JSON_H__

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <json-c/json.h>
#include "forecast.h"

/*
arguments: json object, name of the object we are looking for, city_st and index for city.forecasts
stores object to coresponding city_st data
*/
void json_parse(json_object *jobj, char *object, city_st *city, int index);
/*
arguments: path to json file that made function "autocomplete" and autocomp
*/
bool parse_autocomplete(char *path, autocomp *autocomp);

/*
converts date to week day: 0 - Sun, 1 - Mon ...
*/
int get_wday(int day, int month);

/*
arguments: path to json file that made function "get_city", city_st
*/
bool parse_weather(char *path, city_st *city);
#endif