#include "pictures.h"
#include "lcd_draw.h"
#include "forecast.h"

extern colors_st colors;

void draw_sun(int x, int y)
{
    draw_circle(x + 30, y + 30, 15, colors.white);
    draw_line(x + 5, y + 30, x + 10, y + 30, 3, colors.white);
    draw_line(x + 55, y + 30, x + 50, y + 30, 3, colors.white);
    draw_rect(x + 29, y + 5, x + 31, y + 10, colors.white);
    draw_rect(x + 29, y + 55, x + 31, y + 50, colors.white);
    draw_line(x + 8, y + 8, x + 12, y + 12, 3, colors.white);
    draw_line(x + 52, y + 52, x + 48, y + 48, 3, colors.white);
    draw_line(x + 8, y + 52, x + 12, y + 48, 3, colors.white);
    draw_line(x + 52, y + 8, x + 48, y + 12, 3, colors.white);
}

void draw_snow(int x, int y)
{
    draw_rect(x + 5, y + 28, x + 55, y + 32, colors.white);
    draw_rect(x + 28, y + 5, x + 32, y + 55, colors.white);
    //above
    draw_line(x + 16, y + 11, x + 30, y + 18, 2, colors.white);
    draw_line(x + 44, y + 11, x + 30, y + 18, 2, colors.white);
    //right
    draw_line(x + 49, y + 44, x + 39, y + 30, 2, colors.white);
    draw_line(x + 49, y + 16, x + 39, y + 30, 2, colors.white);
    //bellow
    draw_line(x + 16, y + 49, x + 30, y + 42, 2, colors.white);
    draw_line(x + 44, y + 49, x + 30, y + 42, 2, colors.white);
    //left
    draw_line(x + 11, y + 16, x + 18, y + 30, 2, colors.white);
    draw_line(x + 11, y + 44, x + 18, y + 30, 2, colors.white);
}

void draw_cloud(int x, int y)
{
    draw_circle(x + 15, y + 30, 10, colors.white);
    draw_circle(x + 27, y + 20, 13, colors.white);
    draw_circle(x + 40, y + 25, 15, colors.white);
}

void draw_rain(int x, int y)
{
    draw_cloud(x, y);
    for (int i = 0; i < 8; ++i)
    {
        draw_line(i * 4 + x + 15, y + 44, i * 4 + x + 17, y + 51, 1, colors.white);
    }
}

void draw_weather(int x, int y, char weather)
{
    switch (weather)
    {
    case SUN:
        draw_sun(x, y);
        break;
    case CLOUD:
        draw_cloud(x, y);
        break;
    case RAIN:
        draw_rain(x, y);
        break;
    case SNOW:
        draw_snow(x, y);
        break;
    }
}