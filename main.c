#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#include "serialize_lock.h"
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include "hw_utils.h"
#include "lcd_draw.h"
#include "usr_input.h"
#include "pictures.h"
#include "forecast.h"
#include "parse_json.h"
#include "data_transfer.h"
#include "search.h"

pthread_t auto_thread;
pthread_mutex_t data_mtx;
pthread_cond_t data_cond;

extern colors_st colors;

int main(int argc, char *argv[])
{
    rgb_st red_warning = {.r = 255, .g = 0, .b = 0};
    rgb_st blu_warning = {.r = 0, .g = 0, .b = 200};
    pthread_mutex_init(&data_mtx, NULL);
    pthread_cond_init(&data_cond, NULL);
    hw_init();
    cities_init();
    init_colors();
    autocomp autocomplete = {.cities_quant = 0, .quit = false, .limit = AUTOCOMPLETE_LIMIT};
    city_st city;
    pthread_create(&auto_thread, NULL, autocomplete_thread, &autocomplete);

    while (!autocomplete.quit)
    {
        rgb_led(1, red_warning);
        rgb_led(2, red_warning);
        city = choose_city(&autocomplete);
        rgb_led(1, blu_warning);
        rgb_led(2, blu_warning);
        get_weather_data(city.id);
        parse_weather(WEATHER_PATH, &city);
        show_forecast(city);
        while (true)
        {
            if (get_knob_button_state(RED))
            {
                pthread_mutex_lock(&data_mtx);
                autocomplete.quit = true;
                pthread_mutex_unlock(&data_mtx);
                break;
            }
            else if (get_knob_button_state(GREEN))
            {
                while (get_knob_button_state(GREEN))
                    ;
                /*wait until button is released*/
                break; //choose different city
            }
            else if (get_knob_button_state(BLUE))
            { //refresh data
                rgb_led(1, red_warning);
                rgb_led(2, red_warning);
                get_weather_data(city.id);
                parse_weather(WEATHER_PATH, &city);
                show_forecast(city);
            }
        }
    }

    pthread_cond_broadcast(&data_cond);
    pthread_join(auto_thread, NULL);
    fill_display(colors.black);
    update_display();
    return 0;
}