#ifndef __FORECAST__
#define __FORECAST__

#define CITY_NAME_LIMIT 100
#define AUTOCOMPLETE_LIMIT 8
#define NUMBER_OF_RESULTS_FROM_FORECAST 40

enum weather
{
    SUN,
    CLOUD,
    RAIN,
    SNOW
};

typedef struct forecast_st
{
    char weather;    //number 1 - 3: 0 = sun, 1 = cloud...., 2, 3
    int temperature; //Celsius
    char day;        //number 0 - 6: 0 = sun, 1 = mon, 2 = tue
    char time;       //number 1 - 8: 0 = 0AM, 1 = 3AM, 2 = 6AM
} forecast_st;

typedef struct city_st
{
    char name[CITY_NAME_LIMIT];
    int id;
    forecast_st forecasts[NUMBER_OF_RESULTS_FROM_FORECAST];
} city_st;

typedef struct autocomp
{
    char input[20];
    city_st cities[AUTOCOMPLETE_LIMIT];
    int cities_quant;
    int limit;
    int quit;
} autocomp;

void show_forecast(city_st city);

void celsius_to_leds(int temperature);

#endif