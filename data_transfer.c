#include "data_transfer.h"
#include <string.h>

void get_autocomplete_data(char *input, int limit)
{
    CURL *url = curl_easy_init();
    FILE *fp = fopen(AUTOCOMPLETE_PATH, "wb");
    CURLUcode result = 0;
    char *urlStr;

    //search argument
    char *search = malloc(strlen("search=") + strlen(input) + 2);
    strcpy(search, "search=");
    strcat(search, input);

    //limit argument
    char lim[10];
    char tmp[3];
    sprintf(tmp, "%d", limit);
    strcpy(lim, "limit=");
    strcat(lim, tmp);

    //modify url for API
    if (!result)
    {
        result = curl_url_set(url, CURLUPART_URL, "https://api.teleport.org/api/cities/", 0);
        result = curl_url_set(url, CURLUPART_QUERY, search, CURLU_APPENDQUERY);
        result = curl_url_set(url, CURLUPART_QUERY, lim, CURLU_APPENDQUERY);
        // Convert URL to string for printing
        result = curl_url_get(url, CURLUPART_URL, &urlStr, 0);
    }

    curl_easy_setopt(url, CURLOPT_CUSTOMREQUEST, "GET");
    curl_easy_setopt(url, CURLOPT_URL, urlStr);

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Accept: application/vnd.teleport.v1+json");
    curl_easy_setopt(url, CURLOPT_HTTPHEADER, headers); //append headers
    curl_easy_setopt(url, CURLOPT_WRITEDATA, fp);       //write data to file

    CURLcode ret = curl_easy_perform(url); //perform action
    free(search);
    curl_easy_cleanup(url);
    fclose(fp);
}

void get_weather_data(int id)
{
    CURL *url = curl_easy_init();
    FILE *fp = fopen(WEATHER_PATH, "wb");
    char *urlStr;
    CURLUcode result = 0;

    char city_id[20];
    char tmp[15];
    sprintf(tmp, "%d", id);
    strcpy(city_id, "id=");
    strcat(city_id, tmp);

    //modify url for API
    if (!result)
    {
        result = curl_url_set(url, CURLUPART_URL, "https://community-open-weather-map.p.rapidapi.com/", 0);
        result = curl_url_set(url, CURLUPART_PATH, "forecast", 0);
        result = curl_url_set(url, CURLUPART_QUERY, city_id, CURLU_APPENDQUERY);
        // Convert URL to string for printing
        result = curl_url_get(url, CURLUPART_URL, &urlStr, 0);
    }

    curl_easy_setopt(url, CURLOPT_CUSTOMREQUEST, "GET");
    curl_easy_setopt(url, CURLOPT_URL, urlStr);

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "x-rapidapi-key: 2326493402mshbca2c17bc8f698bp1b2730jsn9f03463afe5d");
    headers = curl_slist_append(headers, "x-rapidapi-host: community-open-weather-map.p.rapidapi.com");
    curl_easy_setopt(url, CURLOPT_HTTPHEADER, headers); //append headers
    curl_easy_setopt(url, CURLOPT_WRITEDATA, fp);       //write data to file

    CURLcode ret = curl_easy_perform(url); //perform action
    curl_easy_cleanup(url);
    fclose(fp);
}
