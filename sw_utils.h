#ifndef __SW_UTILS__
#define __SW_UTILS__

void swap_int(int *a, int *b);

int max(int a, int b);

int min(int a, int b);

void clear_char_ar(char *ar, int len);

#endif