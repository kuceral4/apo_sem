#ifndef __PICTURES__
#define __PICTURES__

#define PIC_SIZE 60

void draw_sun(int x, int y);

void draw_snow(int x, int y);

void draw_cloud(int x, int y);

void draw_rain(int x, int y);

void draw_weather(int x, int y, char weather);

#endif