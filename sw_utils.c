#include "sw_utils.h"

void swap_int(int *a, int *b)
{
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

int max(int a, int b)
{
    return (a > b) ? a : b;
}

int min(int a, int b)
{
    return (a < b) ? a : b;
}

void clear_char_ar(char *ar, int len)
{
    for (int i = 0; i < len; ++i)
    {
        ar[i] = 0;
    }
}