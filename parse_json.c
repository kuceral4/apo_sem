
#include "parse_json.h"
#include "convert_str.h"
#include <time.h>

#define URL_LEN 47
#define MONTH_START 5
#define DAY_START 8
#define TIME_START 11
#define DATA_LEN 2

void json_parse(json_object *jobj, char *object, city_st *city, int index)
{
    enum json_type type;
    char str[CITY_NAME_LIMIT];
    int temp;
    json_object_object_foreach(jobj, key, val) //loops through every object
    {
        if (!strcmp(key, object))
        {
            type = json_object_get_type(val);
            switch (type)
            {
            case json_type_string:
                strcpy(str, json_object_get_string(val));
                if (!strcmp(key, "matching_full_name")) //name
                {
                    convert_string(str, CITY_NAME_LIMIT);
                    strcpy(city->name, str);
                }
                else if (!strcmp(key, "href")) //id
                {
                    char id[(strlen(str) - URL_LEN + 1)];
                    memcpy(id, &str[URL_LEN - 1], (strlen(str) - URL_LEN));
                    id[(strlen(str) - URL_LEN)] = '\0';
                    city->id = atoi(id);
                }
                else if (!strcmp(key, "dt_txt")) //date
                {
                    char month[DATA_LEN + 1] = {0};
                    memcpy(month, &str[MONTH_START], DATA_LEN);
                    char mday[DATA_LEN + 1] = {0};
                    memcpy(mday, &str[DAY_START], DATA_LEN);
                    city->forecasts[index].day = get_wday(atoi(mday), atoi(month));
                    char time[DATA_LEN + 1] = {0};
                    memcpy(time, &str[TIME_START], DATA_LEN);
                    city->forecasts[index].time = atoi(time) / 3;
                }
                else if (!strcmp(key, "main")) //weather
                {
                    if (!strcmp(str, "Clear"))
                    {
                        city->forecasts[index].weather = 0;
                    }
                    else if (!strcmp(str, "Clouds"))
                    {
                        city->forecasts[index].weather = 1;
                    }
                    else if (!strcmp(str, "Rain"))
                    {
                        city->forecasts[index].weather = 2;
                    }
                    else if (!strcmp(str, "Snow"))
                    {
                        city->forecasts[index].weather = 3;
                    }
                    else
                    {
                        fprintf(stderr, "ERROR: Undefined weather type.\n");
                    }
                }
                break;
            case json_type_int:
                temp = json_object_get_int(val) - 272.15;
                city->forecasts[index].temperature = temp;
                break;
                break;
            case json_type_double:
                temp = json_object_get_double(val) - 272.15;
                city->forecasts[index].temperature = temp;
                break;
            default:
                fprintf(stderr, "ERROR: Json_parse.\n");
            }
        }
    }
}
bool parse_autocomplete(char *path, autocomp *autocomp)
{
    bool ret = false;
    FILE *fp = fopen(path, "r"); //json file

    //find length of the file
    size_t pos = ftell(fp);
    fseek(fp, 0, SEEK_END);
    size_t length = ftell(fp);
    fseek(fp, pos, SEEK_SET);

    //store file to buffer
    char *buffer = malloc(length);
    if (fread(buffer, length, 1, fp) < 1)
    {
        fprintf(stderr, "ERROR: Reading %s", path);
    }
    fclose(fp);

    //parse object
    json_object *jobj = json_tokener_parse(buffer);

    struct json_object *em = NULL;             //_embedded
    struct json_object *search_results = NULL; //city:search-results
    struct json_object *item = NULL;           //item
    struct json_object *links = NULL;          //links
    struct json_object *city_item = NULL;      //city:item

    //gets city:search results
    json_object_object_get_ex(jobj, "_embedded", &em);
    json_object_object_get_ex(em, "city:search-results", &search_results);

    if (search_results) //valid data
    {
        ret = true;

        int num_of_results = json_object_array_length(search_results);
        autocomp->cities_quant = num_of_results;

        for (int i = 0; i < num_of_results; ++i)
        {
            item = json_object_array_get_idx(search_results, i);
            json_parse(item, "matching_full_name", &autocomp->cities[i], 0); //parse name
        }
        for (int i = 0; i < num_of_results; ++i)
        {
            item = json_object_array_get_idx(search_results, i);
            json_object_object_get_ex(item, "_links", &links);
            json_object_object_get_ex(links, "city:item", &city_item);
            json_parse(city_item, "href", &autocomp->cities[i], 0); //parse id
        }
    }
    free(buffer);
    return ret;
}

int get_wday(int day, int month)
{
    time_t T = time(NULL);
    struct tm tm = *localtime(&T); //today's data
    int days_in_month[12] = {31, 28, 31, 30, 31, 30,
                             31, 31, 30, 31, 30, 31};
    int ret;

    if (month == tm.tm_mon + 1) //same month
    {
        ret = (day - tm.tm_mday + tm.tm_wday) % 7;
    }
    else if (month == tm.tm_mon + 2) //next month
    {
        ret = (day + days_in_month[tm.tm_mon] - tm.tm_mday + tm.tm_wday) % 7;
    }
    else
    {
        ret = -1;
    }
    return ret;
}

bool parse_weather(char *path, city_st *city)
{
    bool ret = false;
    FILE *fp = fopen(path, "r"); //json file

    //find length of the file
    size_t pos = ftell(fp);
    fseek(fp, 0, SEEK_END);
    size_t length = ftell(fp);
    fseek(fp, pos, SEEK_SET);

    //store file to buffer
    char *buffer = malloc(length);
    if (fread(buffer, length, 1, fp) < 1)
    {
        fprintf(stderr, "ERROR: Reading %s", path);
    }
    fclose(fp);

    //parse object
    json_object *jobj = json_tokener_parse(buffer);

    struct json_object *list = NULL;
    struct json_object *item = NULL;
    struct json_object *main = NULL;
    struct json_object *weather = NULL;
    struct json_object *weather_item = NULL;

    json_object_object_get_ex(jobj, "list", &list);

    if (list) //data are valid
    {
        ret = true;
        int num_of_results = json_object_array_length(list);

        for (int i = 0; i < num_of_results; ++i)
        {
            item = json_object_array_get_idx(list, i);

            json_parse(item, "dt_txt", city, i); //parse date

            json_object_object_get_ex(item, "main", &main);
            json_parse(main, "temp", city, i); //parse temperature

            json_object_object_get_ex(item, "weather", &weather);
            weather_item = json_object_array_get_idx(weather, 0);
            json_parse(weather_item, "main", city, i); //parse weather
        }
    }

    free(buffer);
    return ret;
}