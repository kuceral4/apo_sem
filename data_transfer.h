#ifndef __DATA_TRASFER_H__
#define __DATA_TRASFER_H__

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <string.h>

#define AUTOCOMPLETE_PATH "data/autocomplete.json"
#define WEATHER_PATH "data/weather.json"

/*
arguments: string from user input and limit of matching names
saves json file from API to path defined in macro
*/
void get_autocomplete_data(char *input, int limit);

/*
arguments: city id and boolean - if true function downloads forecast for 5 days
if false function downloads current weather stats

saves json file from API to path defined in macro
*/
void get_weather_data(int id);
#endif