
#include "forecast.h"
#include "lcd_draw.h"
#include "hw_utils.h"
#include "pictures.h"

extern colors_st colors;

void show_forecast(city_st city)
{
    rgb_st led_color = {.r = 255, .g = 0, .b = 0};
    rgb_led(1, led_color);
    rgb_led(2, led_color);
    char *week_days[] = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    fill_display(colors.black);
    update_display();
    int j = 0;
    _Bool end_now = 0;
    while (city.name[j] != '\0')
    {
        if (city.name[j] == ',' && end_now)
        {
            city.name[j] = '\0';
            break;
        }
        else if (city.name[j] == ',')
        {
            end_now = 1;
            ++j;
        }
        else
        {
            ++j;
        }
    }
    lcd_prnt_str(3, 3, city.name, 2.5, colors.white, FALSE);
    draw_rect(0, FONT.height * 2.5 + 1, LCD_WIDTH, FONT.height * 2.5 + 4, colors.white);
    draw_rect(0, LCD_HEIGHT / 2, LCD_WIDTH, LCD_HEIGHT / 2 + 2, colors.white);
    for (int i = 0; i < 5; ++i)
    {
        int y = 1 + FONT.height * 2.5;
        draw_rect(i * LCD_WIDTH / 5 - 1, y, i * LCD_WIDTH / 5 + 1, LCD_HEIGHT / 2, colors.white);
        draw_weather(5 + i * LCD_WIDTH / 5, y + 3, city.forecasts[i * 8].weather);
        y += PIC_SIZE;
        lcd_prnt_int(5 + i * LCD_WIDTH / 5, y, city.forecasts[i * 8].temperature, 2, colors.white);
        lcd_prnt_str(6 + i * LCD_WIDTH / 5 + 2 * FONT.maxwidth * 2, y, "'C", 2, colors.white, FALSE);
        y += FONT.height * 2 + 1;
        lcd_prnt_str(5 + i * LCD_WIDTH / 5, y, week_days[(int)city.forecasts[i * 8].day], 2, colors.white, FALSE);
    }
    for (int i = 0; i < 8; ++i)
    {
        int y = LCD_HEIGHT / 2;
        int x = i * LCD_WIDTH / 8;
        draw_rect(x - 1, y, x + 1, LCD_HEIGHT, colors.white);
        draw_weather(x, y, city.forecasts[i].weather);
        x += 7;
        y += PIC_SIZE;
        lcd_prnt_int(x, y, city.forecasts[i].temperature, 1.5, colors.white);
        lcd_prnt_str(x + 2 + FONT.maxwidth * 1.5, y, "'C", 1.5, colors.white, FALSE);
        y += FONT.height * 1.5;
        lcd_prnt_str(x, y, week_days[(int)city.forecasts[i].day], 1.5, colors.white, FALSE);
        y += FONT.height * 1.5;
        int time = city.forecasts[i].time * 3;
        if (time > 12)
        {
            lcd_prnt_int(x, y, time % 13 + 1, 1.2, colors.white); // %13 + 1 so that 12 remains 12 but 13 converts to 1
            lcd_prnt_str(x + 2 * FONT.maxwidth * 1.2, y, "PM", 1.5, colors.white, FALSE);
        }
        else
        {
            lcd_prnt_int(x, y, time, 1.2, colors.white);
            lcd_prnt_str(x + 2 * FONT.maxwidth * 1.2, y, "AM", 1.5, colors.white, FALSE);
        }
    }
    update_display();
    sleep(1);
    celsius_to_leds(city.forecasts[0].temperature);
}

void celsius_to_leds(int temperature)
{
    char leds[32];
    clear_char_ar(leds, 32);
    if (temperature > 32)
    {
        temperature = 32;
    }
    for (int i = 0; i < temperature; ++i)
    {
        leds[i] = TRUE;
    }
    led_line(leds);
    rgb_st rgb = {.r = 0, .g = 0, .b = 0};
    if (temperature > 30)
    {
        rgb.r = 255;
    }
    else if (temperature > 15)
    {
        rgb.r = 255;
        double tmp = 255 / (30 - 15);
        rgb.g = (30 - temperature) * tmp;
    }
    else if (temperature > 0)
    {
        rgb.r = rgb.g = 255;
        double tmp = 255 / 15;
        rgb.b = (15 - temperature) * tmp;
    }
    else
    {
        rgb.r = rgb.g = rgb.b = 255;
    }
    rgb_led(1, rgb);
    rgb_led(2, rgb);
}