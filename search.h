#ifndef __SEARCH__
#define __SEARCH__
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>

#include "usr_input.h"
#include "hw_utils.h"
#include "lcd_draw.h"
#include "forecast.h"
#include "data_transfer.h"
#include "parse_json.h"

#define NO_OF_SLOTS 10

void search(autocomp *autocomp);

void *autocomplete_thread(void *arg);

city_st *select_name(int slot_id, autocomp autocomplete);

city_st choose_city(autocomp *autocomplete);

void cities_init();

#endif