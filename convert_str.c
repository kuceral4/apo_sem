#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "convert_str.h"

void convert_character(char *str, char *ptr, char *c, int max_len)
{
    if (ptr && str)
    {
        char tmp1[max_len];
        char tmp2[max_len];
        strcpy(tmp1, ptr + 2);
        *ptr = '\0';
        strcpy(tmp2, str);
        strcat(tmp2, c);
        strcat(tmp2, tmp1);
        strcpy(str, tmp2);
    }
}

void convert_string(char *str, int max_len)
{
    char *ptr;
    while ((ptr = strstr(str, "ě")))
    {
        convert_character(str, ptr, "e", max_len);
    }
    while ((ptr = strstr(str, "š")))
    {
        convert_character(str, ptr, "s", max_len);
    }
    while ((ptr = strstr(str, "č")))
    {
        convert_character(str, ptr, "c", max_len);
    }
    while ((ptr = strstr(str, "ř")))
    {
        convert_character(str, ptr, "r", max_len);
    }
    while ((ptr = strstr(str, "ž")))
    {
        convert_character(str, ptr, "z", max_len);
    }
    while ((ptr = strstr(str, "ý")))
    {
        convert_character(str, ptr, "y", max_len);
    }
    while ((ptr = strstr(str, "á")))
    {
        convert_character(str, ptr, "a", max_len);
    }
    while ((ptr = strstr(str, "í")))
    {
        convert_character(str, ptr, "i", max_len);
    }
    while ((ptr = strstr(str, "é")))
    {
        convert_character(str, ptr, "e", max_len);
    }
    while ((ptr = strstr(str, "ú")))
    {
        convert_character(str, ptr, "u", max_len);
    }
    while ((ptr = strstr(str, "ů")))
    {
        convert_character(str, ptr, "u", max_len);
    }
    while ((ptr = strstr(str, "Ě")))
    {
        convert_character(str, ptr, "E", max_len);
    }
    while ((ptr = strstr(str, "Š")))
    {
        convert_character(str, ptr, "S", max_len);
    }
    while ((ptr = strstr(str, "Č")))
    {
        convert_character(str, ptr, "C", max_len);
    }
    while ((ptr = strstr(str, "Ř")))
    {
        convert_character(str, ptr, "R", max_len);
    }
    while ((ptr = strstr(str, "Ž")))
    {
        convert_character(str, ptr, "Z", max_len);
    }
    while ((ptr = strstr(str, "Ý")))
    {
        convert_character(str, ptr, "Y", max_len);
    }
    while ((ptr = strstr(str, "Á")))
    {
        convert_character(str, ptr, "A", max_len);
    }
    while ((ptr = strstr(str, "Í")))
    {
        convert_character(str, ptr, "I", max_len);
    }
    while ((ptr = strstr(str, "É")))
    {
        convert_character(str, ptr, "E", max_len);
    }
    while ((ptr = strstr(str, "Ú")))
    {
        convert_character(str, ptr, "Ú", max_len);
    }
    while ((ptr = strstr(str, "Ů")))
    {
        convert_character(str, ptr, "Ů", max_len);
    }
    int i = 0;
    while ((str[i] != '\0') && (i < max_len))
    {
        if (str[i] < 0)
        {
            convert_character(str, &str[i], "?", max_len);
        }
        i++;
    }
}