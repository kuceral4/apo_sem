#include <stdio.h>
#include <pthread.h>

#include "usr_input.h"
#include "hw_utils.h"
#include "lcd_draw.h"
#include "forecast.h"
#include "search.h"

#define STEP 4
#define ROWS 4
#define COLS 8
#define KEYBOARD_HGHT 160

extern colors_st colors;
city_st slots[NO_OF_SLOTS];

void keyboard_prep()
{
    draw_rect(156, LCD_HEIGHT - KEYBOARD_HGHT, LCD_WIDTH, LCD_HEIGHT, colors.black);
    for (int i = 0; i < ROWS + 1; i++)
    {
        draw_rect(156, LCD_HEIGHT - 2 - i * KEYBOARD_HGHT / ROWS, LCD_WIDTH, LCD_HEIGHT - i * KEYBOARD_HGHT / ROWS, colors.white);
    }
    for (int i = 0; i < COLS + 1; ++i)
    {
        if ((i < 3 || i > 5) && (i != 7))
        {
            draw_rect(i * LCD_WIDTH / 12 + 157, KEYBOARD_HGHT, i * LCD_WIDTH / 12 + 159, LCD_HEIGHT, colors.white);
        }
        else
        {
            draw_rect(i * LCD_WIDTH / 12 + 157, KEYBOARD_HGHT, i * LCD_WIDTH / 12 + 159, LCD_HEIGHT - KEYBOARD_HGHT / ROWS, colors.white);
        }
    }
}

char keyboard()
{
    _Bool shift = FALSE;
    int selected, red_k, blue_k;
    keyboard_prep();
    while (TRUE)
    {
        red_k = get_knob_state(RED);
        blue_k = get_knob_state(BLUE);
        selected = COLS - 1 - ((blue_k / STEP) % COLS) + COLS * ((red_k / STEP) % ROWS);
        for (int i = 0; i < 26; ++i)
        {
            int x = 174 + (i % COLS) * LCD_WIDTH / 12;
            int y = LCD_HEIGHT - KEYBOARD_HGHT + i / COLS * KEYBOARD_HGHT / ROWS + 6;
            char letter = shift ? 'A' + i : 'a' + i;
            if (i != selected)
            {
                lcd_prnt_char(x, y, letter, 1.5, colors.white);
            }
            else
            {
                lcd_prnt_char(x, y, letter, 1.5, colors.brown);
            }
        }
        if (selected >= 26 && selected <= 29)
        {
            lcd_prnt_str(174 + 3 * LCD_WIDTH / 12, LCD_HEIGHT - KEYBOARD_HGHT / ROWS + 4, "space", 1.3, colors.brown, FALSE);
        }
        else
        {
            lcd_prnt_str(174 + 3 * LCD_WIDTH / 12, LCD_HEIGHT - KEYBOARD_HGHT / ROWS + 4, "space", 1.3, colors.white, FALSE);
        }
        if (selected > 29)
        {
            lcd_prnt_str(174 + 6 * LCD_WIDTH / 12, LCD_HEIGHT - KEYBOARD_HGHT / ROWS + 4, "shft", 1.3, colors.brown, FALSE);
        }
        else
        {
            lcd_prnt_str(174 + 6 * LCD_WIDTH / 12, LCD_HEIGHT - KEYBOARD_HGHT / ROWS + 4, "shft", 1.3, colors.white, FALSE);
        }
        if (get_knob_button_state(BLUE))
        {
            while (get_knob_button_state(BLUE))
                ;
            if (selected < 26)
            {
                return shift ? 'A' + selected : 'a' + selected;
            }
            else if (selected < 30)
            {
                return ' ';
            }
            else
            {
                shift = !shift;
                keyboard_prep();
            }
        }
        else if (get_knob_button_state(GREEN))
        {
            while (get_knob_button_state(GREEN))
                ;
            return '\0';
        }
        else if (get_knob_button_state(RED))
        {
            while (get_knob_button_state(RED))
                ;
            return '\b';
        }

        update_display();
    }
}
