#include "hw_utils.h"

static unsigned char *spil_mem_base;
static unsigned char *lcd_mem_base;

static pixel_st frame_buf[LCD_WIDTH][LCD_HEIGHT];

_Bool hw_init()
{
    lcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    spil_mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if (!lcd_mem_base || !spil_mem_base)
    {
        return FALSE;
    }
    else
    {
        //parlcd_hx8357_init(lcd_mem_base);
        parlcd_write_cmd(lcd_mem_base, 0x2c);
        pixel_st background = {.rgb = 0};
        fill_display(background);
        update_display();
        return TRUE;
    }
}

//done
//args: spil_mem_base, array of 32 chars (1 or 0 for each led)
void led_line(const char leds[32])
{
    uint32_t led_line_bits = 0;
    for (int i = 0; i < 32; ++i)
    {
        led_line_bits |= leds[i] ? (1 << (31 - i)) : 0;
    }
    *(volatile uint32_t *)(spil_mem_base + SPILED_REG_LED_LINE_o) = led_line_bits;
}

//done
//args: spil_mem_base, lednumber (1 or 2), rgbstruct
void rgb_led(const char led_num, rgb_st color)
{
    if (led_num == 1 || led_num == 2)
    {
        int offset = (led_num == 1) ? SPILED_REG_LED_RGB1_o : SPILED_REG_LED_RGB2_o;
        *(volatile uint8_t *)(2 + spil_mem_base + offset) = color.r;
        *(volatile uint8_t *)(1 + spil_mem_base + offset) = color.g;
        *(volatile uint8_t *)(0 + spil_mem_base + offset) = color.b;
    }
}

//done
void fill_display(pixel_st color)
{
    for (int x = 0; x < LCD_WIDTH; ++x)
    {
        for (int y = 0; y < LCD_HEIGHT; ++y)
        {
            frame_buf[x][y] = color;
        }
    }
}

//done
void update_display()
{
    parlcd_write_cmd(lcd_mem_base, 0x2c);
    for (int y = 0; y < LCD_HEIGHT; ++y)
    {
        for (int x = 0; x < LCD_WIDTH; ++x)
        {
            parlcd_write_data(lcd_mem_base, frame_buf[x][y].rgb);
        }
    }
}

void pixel_write(const int x, const int y, const pixel_st color)
{
    if (x < LCD_WIDTH && y < LCD_HEIGHT && x > 0 && y > 0)
    {
        frame_buf[x][y] = color;
    }
}

uint8_t get_knob_state(const int knob_color)
{
    return *(volatile uint8_t *)(spil_mem_base + SPILED_REG_KNOBS_8BIT_o + knob_color);
}

_Bool get_knob_button_state(const int knob_color)
{
    int offset = 3 + SPILED_REG_KNOBS_8BIT_o; //+3 because bits 0 - 24 are rotation positions
    _Bool ret = (*(volatile uint8_t *)(spil_mem_base + offset) >> knob_color) & 1;
    return ret;
}
