#include "lcd_draw.h"
#include <iconv.h>

colors_st colors;

void init_colors()
{
    colors.black.rgb = 0x0000;
    colors.white.rgb = 0xffff;
    colors.brown.rgb = 0xcccc;
}

void draw_rect(int xstart, int ystart, int xend, int yend, pixel_st color)
{
    if (xstart > xend)
        swap_int(&xstart, &xend);
    if (ystart > yend)
        swap_int(&ystart, &yend);
    for (int x = xstart; x <= xend; ++x)
    {
        for (int y = ystart; y < yend; ++y)
        {
            pixel_write(x, y, color);
        }
    }
}

void draw_line(int xstart, int ystart, int xend, int yend, int width, pixel_st color)
{
    if (xstart > xend)
    {
        swap_int(&xstart, &xend);
        swap_int(&ystart, &yend);
    }
    int y;
    double slope = (double)(yend - ystart) / (double)(xend - xstart);
    for (int x = xstart; x <= xend; ++x)
    {
        y = (int)(ystart + (x - xstart) * slope);
        for (int xtmp = x - width / 2; xtmp <= x + width / 2; ++xtmp)
        {
            for (int ytmp = y - width / 2; ytmp <= y + width / 2; ++ytmp)
            {
                pixel_write(xtmp, ytmp, color);
            }
        }
    }
}

void draw_circle(int xc, int yc, int r, pixel_st color)
{
    for (int x = xc - r; x <= xc + r; ++x)
    {
        for (int y = yc - r; y <= yc + r; ++y)
        {
            if ((x - xc) * (x - xc) + (y - yc) * (y - yc) <= r * r)
                pixel_write(x, y, color);
        }
    }
}

int lcd_prnt_char(int xstart, int ystart, unsigned char c, float scale, pixel_st color)
{
    if (c > 128)
    {
        c = '_'; //defaultchar
    }
    int offset = c - FONT.firstchar;
    if (offset > FONT.size)
    {
        return FALSE; //stop if character not in this font
    }
    int c_width = (FONT.width != NULL) ? FONT.width[offset] : FONT.maxwidth;
    for (int y = 0; y < FONT.height * scale; ++y)
    {
        font_bits_t bit_map = FONT.bits[offset * FONT.height + (int)(y / scale)]; //*font.height because width is the sizeof(font_bits_t)
        for (int x = 0; x < c_width * scale; ++x)
        {
            if (bit_map & (1 << (sizeof(bit_map) * 8 - 1 - (int)(x / scale))))
            {
                pixel_write(x + xstart, y + ystart, color);
            }
        }
    }
    return (c_width * scale);
}

void lcd_prnt_str(int xstart, int ystart, char *str, float scale, pixel_st color, int max_width)
{
    unsigned int len = strlen(str);
    int offset = xstart;
    for (int i = 0; i < len; ++i)
    {
        if (!max_width || max_width >= xstart - offset + FONT.maxwidth * scale)
            xstart += lcd_prnt_char(xstart, ystart, str[i], scale, color);
    }
}

void lcd_prnt_int(int xstart, int ystart, int num, float scale, pixel_st color)
{
    int tmp = num / 10;
    int len = 1;
    while (tmp != 0)
    {
        tmp /= 10;
        len++;
    }
    char num_str[len + 1];
    num_str[len] = '\0';
    for (int i = 0; i < len; ++i)
    {
        num_str[len - 1 - i] = (num % 10) + '0';
        num /= 10;
    }
    lcd_prnt_str(xstart, ystart, num_str, scale, color, FALSE);
}